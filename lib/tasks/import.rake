require 'csv'

namespace :app do
  desc "Import teams from csv"
  task import: :environment do
    raw = CSV.read("#{ENV['EVENTCODE']}.csv")
    raw.each do |l|
      Team.create(number: l[0].to_i, name: l[1])
    end
  end
end
