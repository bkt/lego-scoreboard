Rails.application.routes.draw do
  resource :match_queue, only: [:edit, :update]
  root 'welcome#index'
  resources :teams

  resources :scores

  get 'exportScores', to: 'welcome#export', as: 'export_scores'
  get 'exportGp', to: 'welcome#gp', as: 'export_gp_scores'
  get 'display', to: 'welcome#display', as: 'display'
  get 'pit', to: 'welcome#pit', as: 'pit'
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
end
