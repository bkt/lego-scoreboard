json.extract! score, :id, :team_id, :score, :gp_score, :round, :created_at, :updated_at
json.url score_url(score, format: :json)
