json.extract! match_queue, :id, :name, :number, :created_at, :updated_at
json.url match_queue_url(match_queue, format: :json)
