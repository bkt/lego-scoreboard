json.extract! team, :id, :number, :name, :created_at, :updated_at
json.url team_url(team, format: :json)
