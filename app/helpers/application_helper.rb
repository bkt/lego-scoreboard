module ApplicationHelper
  def icon(icon, variant="solid", *options)
    content_tag(:i, '', class: [
      'fa-fw',
      "fa-#{variant}",
      "fa-#{icon}",
      *options
      ]).html_safe
  end
end
