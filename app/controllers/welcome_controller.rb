class WelcomeController < ApplicationController
  skip_before_action :http_authenticate, only: %i[index display]

  def index
  end

  def display
    render 'display', layout: 'scroller'
  end

  def export
    @teams = Team.order(:number)
    @ranks = Ranker.new.ranks
  end

  def gp
    @teams = Team.order(:number)
    @ranks = Ranker.new.ranks
  end
end
