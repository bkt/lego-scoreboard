class MatchQueuesController < ApplicationController
  before_action :set_match_queue, only: %i[ edit update ]

  # GET /match_queues/1/edit
  def edit
  end

  # PATCH/PUT /match_queues/1 or /match_queues/1.json
  def update
    respond_to do |format|
      if @match_queue.update(match_queue_params)
        format.html { redirect_to edit_match_queue_path, notice: "Match queue was successfully updated." }
        format.json { render :show, status: :ok, location: @match_queue }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @match_queue.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Only allow a list of trusted parameters through.
    def match_queue_params
      params.require(:match_queue).permit(:name, :number)
    end
end
