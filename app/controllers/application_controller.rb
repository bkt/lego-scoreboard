class ApplicationController < ActionController::Base
  before_action :http_authenticate, :set_match_queue

  def http_authenticate
    return true unless ENV['SCOREPASSWORD']
    authenticate_or_request_with_http_basic do |username, password|
        username == 'scoreboard' && password == ENV['SCOREPASSWORD']
    end
  end

  def set_match_queue
    @match_queue = MatchQueue.find_or_create_by(id: 1)
  end
end
