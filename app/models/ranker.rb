class Ranker
  attr_reader :scores
  def initialize
    @scores = []
    Team.all.each do |team|
      max = [team.round1&.score || -1, team.round2&.score || -1, team.round3&.score || -1].sort.reverse
      scores << ([team.number] + max)
    end
    scores.sort_by! { |e| [e[1], e[2], e[3], -e[0]] }
    scores.reverse!

  end

  def teams
    @scores.map { |t| Team.find_by(number: t[0]) }
  end

  def ranks
    scores.map { |s| [ s[0], scores.index(s) + 1 ]}.to_h
  end
end