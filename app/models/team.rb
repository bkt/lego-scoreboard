class Team < ApplicationRecord
  has_many :scores

  def long_name
    "#{number} - #{name}"
  end

  def round1
    scores.round1.first_or_create(score: -1, gp_score: -1, round: 1)
  end

  def round2
    scores.round2.first_or_create(score: -1, gp_score: -1, round: 2)
  end

  def round3
    scores.round3.first_or_create(score: -1, gp_score: -1, round: 3)
  end

  def max
    @max = [round1&.score || -1, round2&.score || -1, round3&.score || -1].max
  end
end
