class Score < ApplicationRecord
  belongs_to :team

  scope :round1, -> { where(round: 1) }
  scope :round2, -> { where(round: 2) }
  scope :round3, -> { where(round: 3) }

  after_update_commit -> { 
    broadcast_render_to :scoreboard, partial: 'welcome/rankRedraw' 
    broadcast_render_to :pit, partial: 'welcome/pitRedraw' 
  }
end
