class MatchQueue < ApplicationRecord
    before_create :confirm_singularity

    def confirm_singularity
        raise Exception.new("There can be only one.") if MatchQueue.count > 0
    end

    after_update_commit -> { broadcast_render_to :match, partial: 'welcome/matchRedraw' }
end
