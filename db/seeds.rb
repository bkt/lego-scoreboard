# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

24.times do |i|
  #Team.create(number: i, name: "Team #{i}")
  MatchQueue.first_or_create(id: 1).update(name: 'Test Mode', number: 1)
end